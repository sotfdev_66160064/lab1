import java.util.Scanner;

public class XOXO {
    public static void main(String[] args) {
        char[][] box = new char[3][3]; 
        char p1 = 'X';  
        Scanner scan = new Scanner(System.in);  
        int move=0;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                box[i][j] = '-';
            }
        }

        while (true) {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    System.out.print(box[i][j]);
                }
                System.out.println();
            }


            int row, col;
            while (true) {
                System.out.println("Turn "+p1);
                System.out.println("Please input (row,col): ");
                row = scan.nextInt() - 1;
                col = scan.nextInt() - 1;
                if (row >= 0 && row < 3 && col >= 0 && col < 3 && box[row][col] == '-') {
                    box[row][col] = p1;
                    move++;
                    break;
                } else {
                    System.out.println("[Error]");
                    System.out.println("Try again.");
                }
            }

            boolean win = false;
            for (int i = 0; i < 3; i++) {
                if ((box[i][0] == p1 && box[i][1] == p1 && box[i][2] == p1) ||
                    (box[0][i] == p1 && box[1][i] == p1 && box[2][i] == p1)) {
                    win = true;
                    break;
                }
            }
            if ((box[0][0] == p1 && box[1][1] == p1 && box[2][2] == p1) ||
                (box[0][2] == p1 && box[1][1] == p1 && box[2][0] == p1)) {
                win = true;
            }
            if (win) {
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        System.out.print(box[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("Game end[" + p1 + " Win!!!]");
                break; 
            }

            if (move == 9) {
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        System.out.print(box[i][j]);
                    }
                    System.out.println();
                }
                System.out.println("The game ended with a draw!");
                break;  
            }

            if (p1 == 'X') {
                p1 = 'O';
            } else {
                p1 = 'X';
            }
        }
    }
}
